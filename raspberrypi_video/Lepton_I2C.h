#ifndef LEPTON_I2C
#define LEPTON_I2C

void lepton_perform_ffc();
void lepton_reboot();
int lepton_connect();
int lepton_GET_sceneEmissivity();
int lepton_SET_sceneEmissivity();
int lepton_GET_TBkgK();
int lepton_SET_TBkgK();

#endif
