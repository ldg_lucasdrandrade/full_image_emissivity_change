**Description**

Este repositório destina-se a documentar uma maneira de escrever e ler alguns parâmetros no Módulo de Radiometria, especificamente nas funções rad flux linear parameters, da câmera FLIR Lepton® 3.5 usando o FLIR Lepton® Camera Breakout Board v2.0 da TELEDYNE.

**Note** 

Usamos como base, uma aplicação já desenvolvida chamada raspberry_video, do LeptonModule. Essa aplicação está disponível no link abaixo.  

https://github.com/groupgets/LeptonModule/tree/master/software/raspberrypi_video 

--- 

# Hardware Setup  

Serão necessários os seguintes itens: 

1. Raspberry Pi versões 1,2,3,4 ou Zero 

2. FLIR Lepton 3.5 

3. FLIR Lepton® Camera Breakout Board v2.0 

4. Jumpers Fêmea-Fêmea 

Para que o aplicativo seja executado corretamente, uma câmera Lepton deve ser anexada de forma específica aos pinos SPI, power e ground da interface GPIO do Raspi, bem como os pinos I2C SDA/SCL:

-   P8 -> SCL -> GPIO 3
-   P5 -> SDA -> GPIO 2
-   P12 -> MISO -> GPIO 9
-   P7 -> CLK -> GPIO 11
-   P10 -> CS -> GPIO 8
-   P15 -> VSYNC -> GPIO 17

>O pino GND de Lepton deve ser conectado ao GND da RasPi.
>O pino CS da Lepton deve ser conectado ao pino CE1 do RasPi.
>O pino MISO do Lepton deve ser conectado ao pino MISO da RasPI.
>O pino CLK do Lepton deve ser conectado ao pino SCLK da RasPI.
>O pino VIN do Lepton deve ser conectado ao pino 3v3 do RasPI.
>O pino SDA do Lepton deve ser conectado ao pino SDA do RasPI.
>O pino SCL do Lepton deve ser conectado ao pino SCL da RasPI.

---

# Software Setup 

--- 

## First step: environment configuration. 

O primeiro passo necessário para escrever e ler parâmetros do Módulo de Radiometria é criar uma nova pasta em sua máquina. 

Para criar a nova pasta use o comando: 

    mkdir <NomedoDiretorio> 

Em seguida entre na pasta usando o comando: 

    cd <NomedoDiretorio> 
---

## Clone the repository

---

O próximo passo é clonar ou baixar o repositório groupgets/LeptonModule disponivel no link:

https://github.com/groupgets/LeptonModule 

Para clonar o repositório basta executar o seguinte comando: 

    git clone https://github.com/groupgets/LeptonModule.git 

Caso a opção escolhida seja baixar o repositório, basta clicar no link abaixo e descompactar os arquivos na basta criada nos passos anteriores: 

https://github.com/groupgets/LeptonModule/archive/refs/heads/master.zip

--- 

## Testing the raspberry_video application 

--- 

O primeiro passo após clonar o repositório é testar a aplicação raspberry_video. 

Para isso é necessário ativar na raspberry pi as interfaces SPI e I2C na raspberry. 

Isso pode ser feito com o comando:

    sudo raspi-config

Uma janela deverá ser exibida. Navegue até Interface Options e ative as conexões SPI e I2C. 

Com a camera conectada ao Breakout Board v2.0 e as ligações feitas na raspberry, navegue até a pasta raspberry_video com o comando: 

    cd LeptonModule/software/raspberrypi_video 

Para permitir a compilação de arquivos QT, instale o pacote 'qt4-dev-tools' com o comando: 

    sudo apt-get install qt4-dev-tools

Para 'buildar' quaisquer dependências SDK, dentro da pasta raspbery_video execute o comando: 

    qmake && make

*Caso seja necessário limpar quaisquer depências "buildadas" o seguinte comando pode ser executado:*

    make sdkclean && make distclean

*Só executar caso seja necessário desmontar quaisquer dependências já construidas.* 

### Running the application 

Depois de executar o comando qmake && make você pode verificar que um executável chamado raspbery_video foi criado.

Para verificar execute, dentro da pasta raspberry_video, o comando: 

    ls 

Deve existir um arquivo executável verde chamado raspbery_video. 

A seguir, para a Raspberry Pi 4 execute o seguinte comando: 

    sudo sh -c "echo performance > /sys/devices/system/cpu/cpufreq/policy0/scaling_governor"

O comando acima não precisa ser executado em caso de estarem sendo utilizadas versões anteriores da Raspberry Pi 4. 

Para as versões da camera FLIR Lepton 3.X execute o comando a seguir: 

    ./raspberry_video -tl 3

--- 

# Printing the rad flux linear parameters on terminal 

## Organizing the environment

O primeiro passo nesta seção é organizar o ambiente que está sendo construido. 

No inicio desta documentação você foi orientado a criar uma pasta chamada <NomedoDiretorio>. Dentro dela você deve ter clonado ou baixado o repositório LeptonModule. 

O seu terminal deve exibir um PATH similar a este: 

    ~/<NomedoDiretorio>/LeptonModule $ 

Você deve mover as pastas /software/raspberry_video e /software/raspberry_libs para o diretório <NomedoDiretorio>. 

Isso pode ser feito com os seguintes comandos: 

Caminhe até a pasta ~/<NomedoDiretorio>/LeptonModule/software e execute os seguintes comandos: 

    sudo mv raspberrypi_video/ <NomedoDiretorio> 

    sudo mv raspberrypi_libs/ <NomedoDiretorio> 

Para confirmar que as pastas foram movidas, execute e comando ls e confirme que elas não se encontram mais no PATH ~/<NomedoDiretorio>/LeptonModule. 

Executando o comando cd ../.. verifique que os diretorios se encontram dentro da pasta <NomedoDiretorio>. 

Em seguida toda a pasta Lepton_Module pode ser apagada, para isso execute o comando: 

    rm -rf LeptonModule 

## Downloading the additional requirements 

Neste ponto, é necessário importar alguns novos arquivos referentes ao modulo radiométrico da câmera FLIR Lepton®. 

Esses novos arquivos estão disponiveis no link abaixo: 

https://novacoast-my.sharepoint.com/:u:/p/slewis/EV7LFwCXzWpMsHh_k7NTSXcBOZ8JRZdCSpI4sxeVQFD62Q?download=1

Ao clicar no link uma pasta chamada RaspberryPi.zip deve ser baixada. 

Basta extrair o conteudo da pasta RaspberryPi para o diretorio <NomedoDiretorio>. 

## Changing the application original code

Agora com a pasta RaspberryPi dentro do diretorio <NomedoDiretorio> navegue até a pasta RaspberryPi/lepton_sdk com o comando: 

    cd RaspberryPi/lepton_sdk 

Agora mova os arquivos LEPTON_RAD.c e LEPTON_RAD.h para a pasta raspberrypi_libs/leptonSDKEmb32PUB. 

Em seguida a pasta RaspberryPi pode ser apagada. Volte para o diretório <NomedoDiretorio> e execute o seguinte comando: 

    rm -rf RaspberryPi/ 

O proximo passo é limpar as dependencias já contruídas. Para isso execute o comando: 

    make sdkclean && make distclean

Agora vamos novamente 'buildar' as dependências com os novos modulos, executando o comando: 

    qmake && make 

Um arquivo chamado  LEPTON_RAD.o deve ter sido gerado. 

Mova ele para a pasta Debug, localizada em

    ~/<NomedoDiretorio>/raspberrypi_libs/Debug

Neste ponto vamos alterar 3 arquivos do código: 

   1. main.cpp 

   2. Lepton_I2C.h 

   3. Lepton_I2C.cpp 

No arquivo main.cpp, abaixo de #include "MyLabel.h"  inclua as seguintes linhas: 

    #include "Lepton_I2C.h"
    #include <iostream>
    #include <string>

    #include "leptonSDKEmb32PUB/LEPTON_RAD.c"

    using namespace std;

Em seguida, dentro da função int main( int argc, char **argv ){ ...... } , após a linha:

    int loglevel = 0; 

abra espaço e insira as seguintes linhas: 


    int lepton_sceneEmissivity_result; // declaração da variavel que vai    receber o valor da função lepton_sceneEmissivity
    lepton_sceneEmissivity_result = lepton_GET_sceneEmissivity(); // armazenando o retorno da função na variavel criada 
    cout<< lepton_sceneEmissivity_result; // printando no console o valor armazenado na variavel

No arquivo Lepton_I2C.h, abaixo da linha void lepton_reboot(); insira o seguinte trecho de código: 

    int lepton_connect();
    int lepton_GET_sceneEmissivity();

No arquivo Lepton_I2C.cpp  antes da linha bool _connected; insira o seguinte trecho de código: 

    #include "leptonSDKEmb32PUB/LEPTON_RAD.h"
    #include "leptonSDKEmb32PUB/LEPTON_AGC.h"

    #include <iostream>
    #include <string>

    using namespace std;

Ainda no arquivo Lepton_I2C.cpp, após a linha LEP_CAMERA_PORT_DESC_T _port;  insira o seguinte trecho de código: 

    LEP_RAD_FLUX_LINEAR_PARAMS_T rad_flux_linear_params;
    LEP_RESULT result; 



Em seguida abaixo da linha LEP_RESULT result;, insira a função abaixo: 

    int lepton_GET_sceneEmissivity() {
    if(!_connected) {
    lepton_connect();
    }
    result = ((LEP_GetRadFluxLinearParams(&_port, &rad_flux_linear_params)));
    return (rad_flux_linear_params.sceneEmissivity); 
    }

    ## Recompiling the application 

Após realizar todas as alterações e salvar todos os arquivos alterados é necessário compilar novamente o código. 

Para isso, basta executar no terminal, dentro da pasta 
    ~/<NomedoDiretorio>/raspberrypi_libs, 
o comando: 

    qmake && make

## Running the refactored application

Feito isto, basta rodar novamente a aplicação com o comando: 

Para camera FLIR Lepton 3.x: 

    ./raspberrypi_video -tl 3 

Para demais versões da camera FLIR Lepton: 

    ./raspberrypi_video 

Ao fechar a janela com a imagem da camera o valor do parâmetro sceneEmissivity retornado pela função deve aparecer ao lado do nomedamaquina@nomeusuário. 

Formato: 

    FunctionReturnpi@raspberrypi: ~/

Notas Interessantes: 



A função lepton_GET_sceneEmissivity() foi desenvolvida com foco em retornar o valor da emissividade total da imagem. 

    int lepton_GET_sceneEmissivity() {
    if(!_connected) {
    lepton_connect();
    }
    result = ((LEP_GetRadFluxLinearParams(&_port, &rad_flux_linear_params)));
    return (rad_flux_linear_params.sceneEmissivity); 
    }

No entanto, com uma pequena alteração é possível retornar o valor dos outros parâmetros da struct LEP_RAD_FLUX_LINEAR_PARAMS_T_TAG.

    typedef struct LEP_RAD_FLUX_LINEAR_PARAMS_T_TAG
    {
       /* Type     Field name              format   default  range       comment*/
       LEP_UINT16  sceneEmissivity;     /* 3.13     8192     [82, 8192] */
       LEP_UINT16  TBkgK;               /* 16.0     30000    [, ]        value in kelvin 100x*/
       LEP_UINT16  tauWindow;           /* 3.13     8192     [82, 8192] */
       LEP_UINT16  TWindowK;            /* 16.0     30000    [, ]        value in kelvin 100x*/
       LEP_UINT16  tauAtm;              /* 3.13     8192     [82, 8192] */
       LEP_UINT16  TAtmK;               /* 16.0     30000    [, ]        value in kelvin 100x*/
       LEP_UINT16  reflWindow;          /* 3.13     0        [0, 8192-tauWindow] */
       LEP_UINT16  TReflK;              /* 16.0     30000    [, ]        value in kelvin 100x*/

    }LEP_RAD_FLUX_LINEAR_PARAMS_T, *LEP_RAD_FLUX_LINEAR_PARAMS_T_PTR; 

Por exemplo: 

Para retornar o valor tauAtm bastaria alterar a função da seguinte forma: 

    int lepton_GET_tauAtm() {
    if(!_connected) {
    lepton_connect();
    }
    result = ((LEP_GetRadFluxLinearParams(&_port, &rad_flux_linear_params)));
    return (rad_flux_linear_params.tauAtm); 
    }

# Setting new values to the rad flux linear parameters and printing them on terminal

## Solving a bug in the sdk files 

No arquivo LEPTON_RAD.h adicione a seguinte linha de código: 

    #define LEP_CID_RAD_FLUX_LINEAR_PARAMS_SET      (LEP_RAD_MODULE_BASE + 0x00BD)

Após a linha: 

    #define LEP_CID_RAD_FLUX_LINEAR_PARAMS          (LEP_RAD_MODULE_BASE + 0x00BC) 

Agora no arquivo LEPTON_RAD.c, altere a variavel result da função LEP_SetRadFluxLinearParams e a deixe como abaixo: 

    LEP_RESULT LEP_SetRadFluxLinearParams(LEP_CAMERA_PORT_DESC_T_PTR portDescPtr,
                                      LEP_RAD_FLUX_LINEAR_PARAMS_T fluxParams)
    {
    LEP_RESULT result = LEP_OK;
    LEP_UINT16 attributeWordLength = 8;

    result = LEP_SetAttribute(portDescPtr,
                             (LEP_COMMAND_ID)LEP_CID_RAD_FLUX_LINEAR_PARAMS_SET,
                             (LEP_ATTRIBUTE_T_PTR)&fluxParams,
                             attributeWordLength);

     return(result);
    }

Note que após a alteração o valor hexadecimal está correto e de acordo com o IDD. 

## Developing the desired functions  

No arquivo Lepton_I2C.cpp insira o seguinte trecho de código abaixo da linha LEP_RAD_FLUX_LINEAR_PARAMS_T rad_flux_linear_params; : 

    LEP_ATTRIBUTE_T attribute;
    LEP_ATTRIBUTE_T_PTR attributePtr;

Abaixo da função lepton_GET_sceneEmissivity() insira a função  lepton_SET_sceneEmissivity(): 

    int lepton_SET_sceneEmissivity() {
    if(!_connected) {
    lepton_connect();
    }
    attribute = 4753; 
    //rad_flux_linear_params.sceneEmissivity = 1000;
    rad_flux_linear_params.sceneEmissivity = attribute;
    result = ((LEP_SetRadFluxLinearParams(&_port, rad_flux_linear_params)));
    return (result); 
    } 

O valor a ser atribuído ao parâmetro deve ser passado a variável attribute neste caso ela recebe o valor 4753. 

Para alterar o valor do parâmetro TBkgK precisamos inserir a função GET deste parâmetro. Para isto abaixo da função lepton_SET_sceneEmissivity() adicione o seguinte trecho de código: 

    int lepton_GET_TBkgK() {
    if(!_connected) {
    lepton_connect();
    }
    result = ((LEP_GetRadFluxLinearParams(&_port, &rad_flux_linear_params)));
    return (rad_flux_linear_params.TBkgK ); 
    }
Em seguida insira o seguinte trecho de código: 

    int lepton_SET_TBkgK() {
    if(!_connected) {
    lepton_connect();
    }
    attribute = 20223; 
    //rad_flux_linear_params.sceneEmissivity = 1000;
    rad_flux_linear_params.TBkgK = attribute;
    result = ((LEP_SetRadFluxLinearParams(&_port, rad_flux_linear_params)));
    return (result); 
    }

O valor da definido na variável attribute é novamente o valor setado para o parâmetro. 

No arquivo Lepton_I2C.h  escreva o seguinte trecho de código abaixo da linha int lepton_GET_sceneEmissivity();

    int lepton_SET_sceneEmissivity();
    int lepton_GET_TBkgK();
    int lepton_SET_TBkgK();

No arquivo main.cpp apague o seguinte trecho de código: 

    int lepton_sceneEmissivity_result; // declaração da variavel que vai receber o valor da função lepton_sceneEmissivity
    lepton_sceneEmissivity_result = lepton_GET_sceneEmissivity(); 
    cout<< lepton_sceneEmissivity_result;

Em seguida insira os seguintes trechos de código abaixo da linha int loglevel = 0;:

    int setp1 = lepton_GET_sceneEmissivity(); 
    cout<< "Retorno do valor sceneEmissivity da camera na struct rad_flux_linear_params: "; /*Valor default sceneEmissivity: 8192*/
    cout<< setp1;
    cout<< "\n";

    int step2 = lepton_SET_sceneEmissivity(); 
    cout<< "Retorno ErrorCode: "; /*error codes estão disponiveis para consulta no arquivo LEPTON_ErrorCodes.h*/
    cout<< step2;
    cout<< "\n"; 

    int step3; /*declaração da variavel que vai receber o valor da função lepton_sceneEmissivity*/
	step3 = lepton_GET_sceneEmissivity(); 
	cout<< "Retorno do novo valor da Emissividade: ";
	cout<< step3;
	cout<< "\n";

Adicione os trechos de código abaixo para o parâmetro TBkgK: 

    int pas1 = lepton_GET_TBkgK(); 
    cout<< "Retorno do valor TBkgK da camera na struct rad_flux_linear_params: ";  /*Valor default TBkgK: 29515 */
    cout<< pas1;
    cout<< "\n";

    int pas2 = lepton_SET_TBkgK(); 
    cout<< "Retorno ErrorCode: ";
    cout<< pas2;
    cout<< "\n";

    int pas3; /*declaração da variavel que vai receber o valor da função lepton_sceneEmissivity*/
    pas3 = lepton_GET_TBkgK(); 
    cout<< "Retorno do novo valor do TBkgK: ";
    cout<< pas3;
    cout<< "\n";

Após salvar o arquivo main.cpp basta compilar novamente a aplicação.

Para isto, dentro do PATH ~/<NomedoDiretorio>/raspberry_video execute o comando:

    qmake && make

Ainda dentro do do diretório /raspberry_video execute o comando:

    ./raspberrypi_video -tl 3. 

Os error codes podem ser consultados no arquivo LEPTON_ErrorCodes.h