#include "Lepton_I2C.h"

#include "leptonSDKEmb32PUB/LEPTON_SDK.h"
#include "leptonSDKEmb32PUB/LEPTON_SYS.h"
#include "leptonSDKEmb32PUB/LEPTON_OEM.h"
#include "leptonSDKEmb32PUB/LEPTON_Types.h"

#include "leptonSDKEmb32PUB/LEPTON_RAD.h"
#include "leptonSDKEmb32PUB/LEPTON_AGC.h"

#include <iostream>
#include <string>

using namespace std;


bool _connected;

LEP_CAMERA_PORT_DESC_T _port;
LEP_RAD_FLUX_LINEAR_PARAMS_T rad_flux_linear_params;
LEP_ATTRIBUTE_T attribute;
LEP_ATTRIBUTE_T_PTR attributePtr; 
LEP_RESULT result;

int lepton_GET_sceneEmissivity() {
if(!_connected) {
lepton_connect();
}
result = ((LEP_GetRadFluxLinearParams(&_port, &rad_flux_linear_params)));
return (rad_flux_linear_params.sceneEmissivity); 
}

int lepton_SET_sceneEmissivity() {
if(!_connected) {
lepton_connect();
}
attribute = 4753; 
//rad_flux_linear_params.sceneEmissivity = 1000;
rad_flux_linear_params.sceneEmissivity = attribute;
result = ((LEP_SetRadFluxLinearParams(&_port, rad_flux_linear_params)));
return (result); 
}

int lepton_GET_TBkgK() {
if(!_connected) {
lepton_connect();
}
result = ((LEP_GetRadFluxLinearParams(&_port, &rad_flux_linear_params)));
return (rad_flux_linear_params.TBkgK ); 
}

int lepton_SET_TBkgK() {
if(!_connected) {
lepton_connect();
}
attribute = 20223; 
//rad_flux_linear_params.sceneEmissivity = 1000;
rad_flux_linear_params.TBkgK = attribute;
result = ((LEP_SetRadFluxLinearParams(&_port, rad_flux_linear_params)));
return (result); 
}

int lepton_connect() {
	LEP_OpenPort(1, LEP_CCI_TWI, 400, &_port);
	_connected = true;
	return 0;
}

void lepton_perform_ffc() {
	if(!_connected) {
		lepton_connect();
	}
	LEP_RunSysFFCNormalization(&_port);
}

//presumably more commands could go here if desired

void lepton_reboot() {
	if(!_connected) {
		lepton_connect();
	}
	LEP_RunOemReboot(&_port);
}
